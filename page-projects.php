<?php
  Use Roots\Sage\Extras;
  $project_list_vars = array (
    'posts_per_page' => -1
  );
?>

<div class="container">
  <div class="section-title">
      <h5>Our Work</h5>
  </div>
  <div class="row justify-content-center">
    <div class="col fade-up">
      <?php Extras\get_component('templates/project-list', $project_list_vars); ?>
    </div>
  </div>
</div>
