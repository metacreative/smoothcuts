<?php
Use Roots\Sage\Extras;
$project_list_vars = array(
  'posts_per_page' => -1
);
if(have_posts()) : while(have_posts()) : the_post() ;
$gallery = get_field('project_gallery');
$nextPost = get_next_post();
$previousPost = get_previous_post();

?>

<div class="container">
  <div class="section-title">
    <h5><?php echo the_title(); ?></h5>
  </div>
</div>

<div class="container project-slider-container">
  <div class="slider-arrows">
    <div class="btn btn-left">
      <div class="arrow icon-angle-left"></div>
    </div>
    <div class="btn btn-right">
      <div class="arrow icon-angle-right"></div>
    </div>
  </div>
  <div class="project-slider">
    <?php foreach($gallery as $image) : ?>
      <div class="item">
        <?php echo Extras\niceImage($image['ID'], 'lazyload'); ?>
      </div>
    <?php endforeach; ?>
  </div>
</div>

<div class="container fade-up">
  <ul class="project-thumbnails">
    <?php foreach($gallery as $image) : ?>
      <li class="thumbnail fit">
        <?php echo Extras\niceThumbnail($image['ID'], ''); ?>
      </li>
    <?php endforeach; ?>
  </ul>
</div>

<div class="container project-description">
  <div class="row">

    <div class="col-md-8 order-md-4 fade-up">
      <p><?php echo the_content(); ?></p>
    </div>

    <div class="col-md-4 order-md-1 fade-up">
      <div class="details">
        <h5>Details</h5>
        <?php echo get_field('project_details'); ?>
        <h5>Location</h5>
        <p><?php echo get_field('project_location'); ?></p>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-8 offset-md-4 fade-up">
      <div class="project-nav">
        <?php if($nextPost != null) : ?>
        <a href="<?= get_permalink( $nextPost->ID ) ?>"> <h5>Previous</h5> </a>
      <?php else : ?>
        <a href="#" class="disabled"> <h5>Previous</h5> </a>
      <?php endif;
       if($previousPost != null) : ?>
        <a href="<?= get_permalink( $previousPost->ID ) ?>"> <h5>Next</h5> </a>
      <?php else : ?>
        <a href="#" class="disabled"> <h5>Next</h5> </a>
      <?php endif; ?>
      </div>
    </div>
  </div>



</div>

<?php
endwhile;
endif;
?>




<div class="container section-padding">
  <div class="section-title">
    <h5>All Projects</h5>
  </div>
  <div class="row justify-content-center">
    <div class="col fade-up">
      <?php Extras\get_component('templates/project-list', $project_list_vars); ?>
    </div>
  </div>
</div>
