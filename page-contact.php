<?php if(have_posts()) : the_post() ; endif ;
$hours = get_field('hours');
$phone = get_field('phone');
$email = get_field('email');
$instagramUrl = get_field('instagram_url');
$facebookUrl = get_field('facebook_url');
$mailhref = "mailto:" . $email;
?>

<div class="container">
  <div class="section-title">
    <h5>Contact Us</h5>
  </div>
  <div class="row">
    <div class="col-md-8 order-md-1 fade-up">
      <?php echo the_content(); ?>
      <br>
      <div class="contact-form section-margin-bottom fade-up">
        <?php echo do_shortcode('[contact-form-7 id="93" title="Contact Page"]'); ?>
      </div>
    </div>
    <div class="col-md-4 contact-sidebar fade-up">
      <h5 class="icon-phone">778-233-8626</h5>
      <h5 class="icon-mail"><a href=<?php echo $mailhref; ?>><?php echo $email; ?></a> </h5>
      <ul>
        <li><a href=<?php echo $instagramUrl; ?> class="icon-instagram">Instagram</a></li>
        <li><a href=<?php echo $facebookUrl; ?> class="icon-facebook">Facebook</a></li>
      </ul>
      <br>
      <?php echo $hours; ?>
      <br>

    </div>
  </div>
</div>

<div class="container section-margin-bottom fade-up">
  <div id="map"></div>
</div>
