/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
/*~~~~~~~~~~~~~~~
VARS
~~~~~~~~~~~~~~~~*/
      var mapsLoaded = false;
      var navLinks = [];
/*~~~~~~~~~~~~~~~
INIT
~~~~~~~~~~~~~~~~*/
      function initScripts() {
        //FIX BODY CLASSES
        $('body').attr('class', String($('classes').attr('class')));
        $('classes').remove();

        //SLICK SLIDER
        $('.slick').not('.slick-initialized').slick({
          arrows: false,
          slidesToShow: 4,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 3000,
          responsive: [
            {
              breakpoint: 1000,
              settings: {
                slidesToShow: 3
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2
              }
            }
          ]
        });
        $('.project-slider').not('.slick-initialized').slick({
          arrows: false,
          fade: true
        });

        //LAZYLOAD
        $('.project-thumbnail img').addClass('lazyload');
        $('.project-thumbnails img').addClass('lazyload');
        document.addEventListener('lazyloaded', function(e) {
          TweenLite.fromTo(e.target, 0.3, {opacity:0}, {opacity:1, delay: 0.5});
        });
        $('.lazyload').css('opacity', '0.001');

        //SCROLLMAGIC
        $('.fade-up').css({
          'opacity': 0,
          'margin-top': '15px'
        });

        var scrollController = new ScrollMagic.Controller();

        $('.fade-up').each(function(index, element){
          var scene = new ScrollMagic.Scene({
            triggerElement: this,
            triggerHook: 0.9,
            duration: 0,
            reverse: true,
            tweenChanges: true
          })
          .setTween(element, 0.5, {opacity: 1, marginTop: '0px'})
          .addTo(scrollController);
        });



        //PROJECT UI
        $('.slider-arrows .btn-left').click(function(){
          $('.project-slider').slick('slickPrev');
        });

        $('.slider-arrows .btn-right').click(function(){
          $('.project-slider').slick('slickNext');
        });

        $('.thumbnail').click(function(){
          $('.project-slider').slick('slickGoTo', $(this).index());
        });

        $('.project-nav a, .project-thumbnail').click(function(){
          scrollToTop();
        });

        $('.project-thumbnail').click(function(){
          deselect();
        });

        //MODERNIZR
        if(!Modernizr.touchevents) {
          $('.project-thumbnail').hover(

            function(){
              var img = $(this).find('img');
              var title = $(this).find('h5');
            TweenLite.to(img, 1, {ease: Power3.easeOut, scale: 1.1, opacity: 0.3});
            TweenLite.to(title, 0.2, {opacity: 1, padding: "20px", border: '1px solid '+fitColorIn()});
          },
            function(){
              var img = $(this).find('img');
              var title = $(this).find('h5');
            TweenLite.to(img, 0.2, {ease: Power3.easeIn, scale: 1, opacity: 1});
            TweenLite.to(title, 0.2, {opacity: 0, padding: "10px", border: '1px solid'+fitColorOut()});
          });
        }

        if(!Modernizr.objectfit){
          $('.fit').each(function(){
            var container = $(this);
            var imgUrl = container.find('img').attr('data-src');
            if (imgUrl) {
              container.css('background-image', 'url(' + imgUrl + ')');
              container.css('background-size', 'cover');
              container.css('background-position', 'center');
              container.find('img').css('display', 'none');
              if(container.find('.title').length) {
                $('h5').css({
                  color: 'white'
                });
              }
            }
          });
        }

        //MAPS
        function initMap() {
          var map;
          var mapStyle = [
              {
                  "featureType": "all",
                  "elementType": "labels.text",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "administrative",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "administrative.province",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "on"
                      }
                  ]
              },
              {
                  "featureType": "administrative.locality",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "on"
                      }
                  ]
              },
              {
                  "featureType": "administrative.neighborhood",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "simplified"
                      }
                  ]
              },
              {
                  "featureType": "administrative.land_parcel",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "landscape",
                  "elementType": "all",
                  "stylers": [
                      {
                          "color": "#e5e8e7"
                      },
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "landscape.man_made",
                  "elementType": "geometry.fill",
                  "stylers": [
                      {
                          "color": "#fffff5"
                      },
                      {
                          "visibility": "on"
                      }
                  ]
              },
              {
                  "featureType": "landscape.natural",
                  "elementType": "geometry.fill",
                  "stylers": [
                      {
                          "color": "#f5f5f2"
                      },
                      {
                          "visibility": "on"
                      }
                  ]
              },
              {
                  "featureType": "poi",
                  "elementType": "labels.icon",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "poi.attraction",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "poi.business",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "poi.government",
                  "elementType": "geometry",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "poi.medical",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "poi.park",
                  "elementType": "all",
                  "stylers": [
                      {
                          "color": "#91b65d"
                      },
                      {
                          "gamma": 1.51
                      }
                  ]
              },
              {
                  "featureType": "poi.park",
                  "elementType": "geometry.fill",
                  "stylers": [
                      {
                          "color": "#c0ecc2"
                      }
                  ]
              },
              {
                  "featureType": "poi.park",
                  "elementType": "labels.icon",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "poi.place_of_worship",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "poi.school",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "poi.sports_complex",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "poi.sports_complex",
                  "elementType": "geometry",
                  "stylers": [
                      {
                          "color": "#c7c7c7"
                      },
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "road",
                  "elementType": "all",
                  "stylers": [
                      {
                          "color": "#ffffff"
                      }
                  ]
              },
              {
                  "featureType": "road",
                  "elementType": "labels",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "road.highway",
                  "elementType": "geometry",
                  "stylers": [
                      {
                          "color": "#ffffff"
                      },
                      {
                          "visibility": "simplified"
                      }
                  ]
              },
              {
                  "featureType": "road.highway",
                  "elementType": "labels.icon",
                  "stylers": [
                      {
                          "color": "#ffffff"
                      },
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "road.arterial",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "simplified"
                      },
                      {
                          "color": "#ffffff"
                      }
                  ]
              },
              {
                  "featureType": "road.arterial",
                  "elementType": "geometry",
                  "stylers": [
                      {
                          "visibility": "simplified"
                      }
                  ]
              },
              {
                  "featureType": "road.local",
                  "elementType": "all",
                  "stylers": [
                      {
                          "color": "#ffffff"
                      },
                      {
                          "visibility": "simplified"
                      }
                  ]
              },
              {
                  "featureType": "road.local",
                  "elementType": "geometry",
                  "stylers": [
                      {
                          "visibility": "on"
                      }
                  ]
              },
              {
                  "featureType": "transit",
                  "elementType": "all",
                  "stylers": [
                      {
                          "visibility": "off"
                      }
                  ]
              },
              {
                  "featureType": "water",
                  "elementType": "all",
                  "stylers": [
                      {
                          "color": "#a0d3d3"
                      }
                  ]
              },
              {
                  "featureType": "water",
                  "elementType": "geometry.fill",
                  "stylers": [
                      {
                          "color": "#d6f3f9"
                      }
                  ]
              }
          ];

          map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: 49.242551, lng: -122.979326},
            zoom: 10,
            disableDefaultUI: true,
            styles: mapStyle
          });

          var radius = new google.maps.Circle({
            strokeOpacity: 0,
            fillColor: '#ffff00',
            fillOpacity: 0.25,
            map: map,
            center: {lat: 49.242551, lng: -123.082745},
            radius: 15500
          });
        }

        if($('#map').length) {
          if(mapsLoaded){
            initMap();
          }else{
            $.loadScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyC8D1jPbHXm22CS976N3nNxHlduYz_oxww", initMap);
            mapsLoaded = true;
          }
        }
        //IF ITS THE HOMEPAGE
        var homeHref = basicHref($('.brand').attr('href'));
        var windowHref = basicHref(window.location.href);
        if(homeHref == windowHref) {
          $('.brand').css('pointer-events', 'none');
        }else{
          $('.brand').css('pointer-events', 'all');
        }
        //~~~~END INITSCRIPTS
      }

/*~~~~~~~~~~~~~~~
HELPERS
~~~~~~~~~~~~~~~~*/
        jQuery.loadScript = function (url, callback) {
          jQuery.ajax({
              url: url,
              dataType: 'script',
              success: callback,
              async: true
          });
        };

        function basicHref(href) {
          return href.replace('http:', '').replace('//', '').replace('www.', '');
        }

        function fitColorIn() {
          if (!Modernizr.objectfit) {
            return "rgba(255,255,255,1)";
          }else{
            return "rgba(0,0,0,1)";
          }
        }

        function fitColorOut() {
          if (!Modernizr.objectfit) {
            return 'rgba(0,0,0,0)';
          }else{
            return 'rgba(255,255,255, 0)';
          }
        }

        function scrollToTop() {
          TweenLite.to('body, html', 0.4, {scrollTop: 0});
        }

/*~~~~~~~~~~~~~~~
LAZY SIZES CONFIG
~~~~~~~~~~~~~~~~*/
        window.lazySizesConfig = window.lazySizesConfig || {};
        window.lazySizesConfig.init = false;
        window.lazySizesConfig.loadHidden = false;
        window.lazySizesConfig.expand = 250;


/*~~~~~~~~~~~~~~~
BARBA CONFIG
~~~~~~~~~~~~~~~~*/
        Barba.Pjax.start();

        var PageTransition = Barba.BaseTransition.extend({
          start: function() {
            Promise
              .all([this.newContainerLoading, this.fadeOut()])
              .then(this.fadeIn.bind(this));
          },

          fadeOut: function() {
            var deferred = Barba.Utils.deferred();
            TweenLite.to(this.oldContainer, 0.4, {autoAlpha: 0, onComplete: function(){
              deferred.resolve();
            }});
            return deferred.promise;
          },

          fadeIn: function() {
            this.done();
            initScripts();
            var $newContainer = $(this.newContainer);
            $newContainer.css({
              'opacity': 0,
              'visibility': 'hidden'
            });
            TweenLite.fromTo($newContainer, 0.4, {autoAlpha: 0,}, {autoAlpha: 1});

          }
        });

        Barba.Pjax.getTransition = function() {
          return PageTransition;
        };

/*~~~~~~~~~~~~~~~
NAV CURRENT ITEM
~~~~~~~~~~~~~~~~*/
  function deselect() {
    TweenLite.fromTo('.desktop-nav .selected', 0.3, {backgroundColor: 'rgba(255,255,0,1)'}, {backgroundColor: 'rgba(255,255,0,0)'});
    $('.selected').removeClass('selected');
  }

  function selectItem(index) {
    deselect();
    var $desktopLink = $('.desktop-nav li').eq(index);
    var $mobileLink = $('.mobile-nav li').eq(index);
    $desktopLink.addClass('selected');
    $mobileLink.addClass('selected');

    TweenLite.fromTo($desktopLink, 0.3, {backgroundColor: 'rgba(255,255,0,0)'}, {color: 'white', backgroundColor: 'rgba(255,255,0,1)'})
  }

  if($('.current-menu-item').length) {
    selectItem($('.current-menu-item').index());
  }

  $('.nav-links a').click(function(){
    var index = $(this).parent().index();
    selectItem(index);
  });

  $('.brand').click(function(){
    deselect();
    $('.brand').css('pointer-events', 'none');
  });



/*~~~~~~~~~~~~~~~
MOBILE NAV
~~~~~~~~~~~~~~~~*/
        var mobilenav = $('.mobile-nav').parent();
        mobilenav.mmenu({
          //options
          navbar: {
            add: false
          },
          hooks: {
            "open:start": () => {
              $('#hamburger').addClass('hamburger-x');
            },
            "close:finish": () => {
              $('#hamburger').removeClass('hamburger-x');
            }
          }
        },{
          //config
          offCanvas: {
            pageSelector: ".big-wrapper"
          }
        });

        var menuApi = mobilenav.data("mmenu");

        $('#hamburger').on('click', ()=>{
          menuApi.open();
        });

        $('.mobile-nav a').on('click', ()=>{
          menuApi.close();
        });

/*~~~~~~~~~~~~~~~
RUN INITSCRIPTS
~~~~~~~~~~~~~~~~*/
        initScripts();
      },

      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired



/*~~~~~~~~~~~~~~~
FOUC
~~~~~~~~~~~~~~~~*/
        setTimeout(function(){
          TweenLite.to('html', 0.2, {opacity: 1});
        },300);

      }
    },
    // Home page
    'contact': {
      init: function() {
        // JavaScript to be fired on the home page

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
