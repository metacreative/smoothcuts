<?php if(have_posts()) : the_post(); endif; ?>
<div class="container section-margin-bottom">
  <div class="section-title">
    <h5>Request A Quote</h5>
  </div>
  <div class="row justify-content-center">
    <div class="col-md-8 center-align fade-up">
      <?php echo the_content(); ?>
    </div>
  </div>
  <div class="row">
    <div class="col fade-up">
      <div class="contact-form">
        <?php echo do_shortcode('[contact-form-7 id="48" title="Contact form 1"]');?>
      </div>
    </div>
  </div>
</div>
