<?php
 Use Roots\Sage\Extras;
 $project_list_vars = array (
   'posts_per_page' => 3
 );

get_template_part('templates/builder');
?>



<div class="container section-padding fade-up">
  <div class="section-title">
    <h5>03 Projects</h5>
  </div>
  <div class="row">
    <div class="col">
      <?php Extras\get_component('templates/project-list', $project_list_vars); ?>
    </div>
  </div>
</div>
