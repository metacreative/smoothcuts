<header class="banner">
  <div class="container">
    <a class="brand" href="<?= esc_url(home_url('/')); ?>"><?php get_template_part('templates/logo'); ?></a>
    <nav class="nav-primary">
      <?php
      if (has_nav_menu('primary_navigation')) :
        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'desktop-nav nav-links']);
      endif;
      ?>
    </nav>
    <div id="hamburger">
      <div class="stroke"></div>
      <div class="stroke"></div>
      <div class="stroke"></div>
    </div>
  </div>
</header>

<?php
if (has_nav_menu('primary_navigation')) :
  wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'mobile-nav nav-links']);
endif;
?>
