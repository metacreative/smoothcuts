<?php
Use Roots\Sage\Extras;

while (have_posts()) : the_post();if(have_rows('rows')) : while(have_rows('rows')) : the_row();
/*======================
    1 Column
======================*/
 if(get_row_layout() == "1_column") {
   $vars = array(
     'section_title' => get_sub_field('section_label'),
     'content' => get_sub_field('content'),
     'full' => get_sub_field('full_width')
   );
   Extras\get_component('templates/one-column', $vars);
 }
 /*======================
    2 Column
 ======================*/
 if(get_row_layout() == "2_column") {
   $vars = array(
     'section_title' => get_sub_field('section_label'),
     'col_1_content' => get_sub_field('column_1_content'),
     'col_2_content' => get_sub_field('column_2_content'),
     'full' => get_sub_field('full_width'),
     'use_pull_quote' => get_sub_field('use_pull_quote'),
     'pull_quote' => get_sub_field('pull_quote')
   );
   Extras\get_component('templates/two-column', $vars);
 }


endwhile; endif;
endwhile;
?>
