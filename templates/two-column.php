<?php if($full) : ?>
  <div class="full-container section-margin-bottom">
<?php endif; ?>
<div class="container section-margin-bottom">
  <div class="section-title">
    <h5><?php echo $section_title; ?></h5>
  </div>
  <?php if($use_pull_quote) : ?>
    <div class="row justify-content-center">
      <div class="col-md-8 center-align pull-quote fade-up">
        <?php echo $pull_quote ?>
      </div>
    </div>
  <?php endif; ?>
  <div class="row">
    <div class="col-md-6 fade-up">
      <?php echo $col_1_content; ?>
    </div>
    <div class="col-md-6 fade-up">
      <?php echo $col_2_content; ?>
    </div>
  </div>
</div>
<?php if($full) : ?>
 </div>
<?php endif; ?>
