<?php get_template_part('templates/logo-slider');
$email = get_field('email', 19);
$mailhref = "mailto:" . $email;
$phone = get_field('phone', 19);
$facebookUrl = get_field('facebook_url', 19);
$instagramUrl = get_field('instagram_url', 19);

?>


<footer class="content-info">
  <div class="container">

    <div class="row inline-flex">
      <div class="col center-align">
        <a href=<?php echo $instagramUrl; ?>><h4 class="il-flex-item icon-instagram"></h4></a>
        <a href=<?php echo $facebookUrl; ?>><h4 class="il-flex-item icon-facebook"></h4></a>
      </div>
    </div>

    <div class="row inline-flex">
      <div class="col center-align">
        <a href=<?php echo $mailhref; ?> class="il-flex-item"><h5 class="icon-mail"><?php echo $email; ?></h5></a>
        <h5 class="icon-phone il-flex-item"><?php echo $phone; ?></h5>
      </div>
    </div>

    <div class="row inline-flex">
      <div class="col center-align">
        <h5><?php echo "© Smoothcuts " . date("Y"); ?></h5>
      </div>
    </div>

  </div>
</footer>
