<?php
Use Roots\Sage\Extras;

$args = array(
  'post_type' => array(
    'project'
  ),
  'posts_per_page' => $posts_per_page
);
$query = new WP_Query($args);
?>



<div class="project-list">
<?php
if($query->have_posts()) :
  while($query->have_posts()) : $query->the_post();
    $thumbID = get_post_thumbnail_id();
    ?>
    <a href="<?= the_permalink(); ?>" class="project-thumbnail">
      <div class="thumb fit">
        <div class="title">
          <h5><?php echo the_title(); ?></h5>

        </div>
        <?php echo Extras\niceThumbnail($thumbID, ''); ?>
      </div>
    </a>
    <?php
  endwhile;
endif;
 ?>
 </div>
