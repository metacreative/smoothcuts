<div class="container">
  <div class="slick">

    <div class="slick-item ">
      <a href="https://www.benjaminmoore.com/en-ca" target="_blank"><img src="<?= get_template_directory_uri() . "/assets/images/benjaminmoore.png" ?>" alt=""></a>
    </div>

    <div class="slick-item">
      <a href="http://www.colorhousepaint.com/" target="_blank"><img src="<?= get_template_directory_uri() . "/assets/images/colorhouse.png" ?>" alt=""></a>
    </div>

    <div class="slick-item">
      <a href="#"><img src="<?= get_template_directory_uri() . "/assets/images/ecowise.png" ?>" alt=""></a>
    </div>

    <div class="slick-item">
      <a href="http://us.farrow-ball.com/" target="_blank"><img src="<?= get_template_directory_uri() . "/assets/images/farrowball.png" ?>" alt=""></a>
    </div>

    <div class="slick-item">
      <a href="http://www.mpi.net/" target="_blank"><img src="<?= get_template_directory_uri() . "/assets/images/mpi.png" ?>" alt=""></a>
    </div>

    <div class="slick-item">
      <a href="https://osmo.ca/" target="_blank"><img src="<?= get_template_directory_uri() . "/assets/images/osmo.png" ?>" alt=""></a>
    </div>

  </div>
</div>
