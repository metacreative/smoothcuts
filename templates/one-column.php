<?php if($full) : ?>
  <div class="full-container section-margin-bottom">
<?php endif; ?>
 <div class="container section-margin-bottom">
   <div class="section-title">
     <h5><?php echo $section_title; ?></h5>
   </div>
   <div class="row justify-content-center">
     <div class="col-md-8 order-md-8 center-align fade-up">
       <?php echo $content; ?>
     </div>
   </div>
 </div>
 <?php if($full) : ?>
  </div>
 <?php endif; ?>
