<?php
  $args = array(
    'post_type' => array(
      'review'
    ),
    'posts_per_page' => -1
  );
  $query = new WP_Query($args);
  ?>


  <div class="container">
    <div class="section-title">
      <h5>Reviews</h5>
    </div>
    <div class="row justify-content-center">
      <div class="col">
        <div class="review-list">
          <?php if($query->have_posts()) : while($query->have_posts()) : $query->the_post(); ?>
              <div class="review fade-up">
                <h5 class="title"><?php echo the_title();?></h5>
                <?php echo the_content(); ?>
              </div>
              <?php endwhile; endif; ?>
        </div>
      </div>
    </div>
  </div>
